import React, { Component } from 'react';
import './LandingPage.css';

class KueRoll extends Component {

  render() {
    return (
      <div>
        <div className="columns">
          <div className="column is-3">
            <div className="card" >
              <div className="card-image pesan">
                <figure className="image is-1by1">
                  <img
                    className="is-square"
                    src="img/kueRoll.jpg"
                  />
                </figure>
              </div>
              <div className="card-content">
                <a className="button is-info">
                  PESAN
                </a>
              </div>
            </div>
          </div>
          <div className="column is-3">
            <div className="card" >
              <div className="card-image pesan">
                <figure className="image is-1by1">
                  <img
                    className="is-square"
                    src="img/kueRoll2.jpg"
                  />
                </figure>
              </div>
              <div className="card-content">
                <a className="button is-info">
                  PESAN
                </a>
              </div>
            </div>
          </div>
          <div className="column is-3">
            <div className="card" >
              <div className="card-image pesan">
                <figure className="image is-1by1">
                  <img
                    className="is-square"
                    src="img/kueRoll3.jpg"
                  />
                </figure>
              </div>
              <div className="card-content">
                <a className="button is-info">
                  PESAN
                </a>
              </div>
            </div>
          </div>
          <div className="column is-3">
            <div className="card" >
              <div className="card-image pesan">
                <figure className="image is-1by1">
                  <img
                    className="is-square"
                    src="img/kueRoll4.jpg"
                  />
                </figure>
              </div>
              <div className="card-content">
                <a className="button is-info">
                  PESAN
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>

    );
  }
}

export default KueRoll;

import React, { Component } from 'react';
import './LandingPage.css';

class Brownies extends Component {

  render() {
    return (
      <div>
        <div className="columns">
          <div className="column is-3">
            <div className="card" >
              <div className="card-image pesan">
                <figure className="image is-1by1">
                  <img
                    className="is-square"
                    src="img/brownies.jpg"
                  />
                </figure>
              </div>
              <div className="card-content">
                <a className="button is-info">
                  PESAN
                </a>
              </div>
            </div>
          </div>
          <div className="column is-3">
            <div className="card" >
              <div className="card-image pesan">
                <figure className="image is-1by1">
                  <img
                    className="is-square"
                    src="img/brownies2.jpg"
                  />
                </figure>
              </div>
              <div className="card-content">
                <a className="button is-info">
                  PESAN
                </a>
              </div>
            </div>
          </div>
          <div className="column is-3">
            <div className="card" >
              <div className="card-image pesan">
                <figure className="image is-1by1">
                  <img
                    className="is-square"
                    src="img/brownies3.jpg"
                  />
                </figure>
              </div>
              <div className="card-content">
                <a className="button is-info">
                  PESAN
                </a>
              </div>
            </div>
          </div>
          <div className="column is-3">
            <div className="card" >
              <div className="card-image pesan">
                <figure className="image is-1by1">
                  <img
                    className="is-square"
                    src="img/brownies4.jpg"
                  />
                </figure>
              </div>
              <div className="card-content">
                <a className="button is-info">
                  PESAN
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>

    );
  }
}

export default Brownies;

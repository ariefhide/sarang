import {
	SET_USER,
	SHOW_MODAL,
	TOGGLE_MODAL,
	SET_LOGIN,
	SET_KOTA,
	USER_LOGOUT,
	SET_START_DATE,
	SET_END_DATE,
	SET_ID_COMPANY,
	SET_BUS_TYPE,
	SET_BUS_ID
} from '../actions/types'

const INITIAL_STATE = {
	user: '',
	modalLogin: false,
	modalRegis:false,
	login:false,
	kota:'',
	startDate:'mulai',
	endDate:'end',
	idCompany:'',
	busType:'',
	busID:''
	
}


export default (state = INITIAL_STATE, action) => {
	switch (action.type) {
		
		case TOGGLE_MODAL:
			return {...state, [action.payload.name]: !state[action.payload.name]}
		case SHOW_MODAL:
			return {...state,
				modalLogin: false,
				modalRegis:false,
				[action.payload.name]: true
			}

		case SET_USER:
			return {...state, user: action.payload.user}

		case SET_START_DATE:
			return {...state, startDate: action.payload.from}

		case SET_END_DATE:
			return {...state, endDate: action.payload.to}		

		case SET_KOTA:
			return {...state, kota: action.payload.kota}

		case SET_ID_COMPANY:
			return {...state, idCompany: action.payload.idCompany}

		case SET_BUS_TYPE:
			return {...state, busType: action.payload.bus}

		case SET_BUS_ID:
			return {...state, busID: action.payload.busID}				
		
		case SET_LOGIN:
            return {...state, login:true}	

            case USER_LOGOUT:
			return {...state, login:false}	
        
		
		default:
			return state
	}
}

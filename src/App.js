import React, { Component } from 'react';
import 'bulma/css/bulma.css';
import './App.css';

import LandingPage from './views/Pages/LandingPage';
import Error from "./views/Pages/Error";

import { BrowserRouter, Route, Switch } from "react-router-dom";


class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div>
          <Switch>
            <Route path="/" component={LandingPage} exact />
            <Route component={Error} />
          </Switch>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
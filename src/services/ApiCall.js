import axios from 'axios'
import { BASE_URL } from '../constants'

axios.defaults.baseURL = BASE_URL

function search(paramEncoded) {
  return axios.get(`/search?${paramEncoded}`)
}



const ApiCall = {
	search
}

export default ApiCall

import React, { Component } from 'react';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';
import { Link } from 'react-router-dom';
import './LandingPage.css';
import Brownies from './Brownies';
import KueLapis from './KueLapis';
import KueRoll from './KueRoll';


class Header extends Component {

  constructor(props) {
    super(props);

    this.state = {
      navbar: false,
      brownies: true,
      lapis: false,
      roll: false

    };
  }
  handleNavbar = () => {
    if (this.state.navbar === false) {
      this.setState({ navbar: true })
    } else {
      this.setState({ navbar: false })
    }
  }

  handleBrownies = () => {
    if (this.state.brownies === false) {
      this.setState({ brownies: true, lapis: false, roll: false })
    }
  }

  handleLapis = () => {
    if (this.state.lapis === false) {
      this.setState({ brownies: false, lapis: true, roll: false })
    }
  }

  handleRoll = () => {
    if (this.state.roll === false) {
      this.setState({ brownies: false, lapis: false, roll: true })
    }
  }

  render() {
    const { navbar } = this.state
    return (
      <div>
        <section>
          <nav className="navbar" role="navigation" aria-label="main navigation" style={{ paddingTop: 10, background: '#8c6145' }}>
            <div className="navbar-brand" style={{ marginLeft: 30 }}>
              <Link className="navbar-item" to="/" style={{ marginTop: -10 }}>
                <b style={{ color: 'white' }}>Sarang Bakery</b>
              </Link>
              <a role="button" onClick={this.handleNavbar} className="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
                <span aria-hidden="true"></span>
                <span aria-hidden="true"></span>
                <span aria-hidden="true"></span>
              </a>
            </div>
            <div id="navbarBasicExample" className={`navbar-menu ${navbar && 'is-active has-text-info has-text-right'}`} ref="toggle">
              <div className="navbar-end" style={{ paddingRight: 50 }}>
                <div className="navbar-item">
                  <Link to="/login"><b style={{ color: 'white' }}>Home</b></Link>
                </div>
                <div className="navbar-item">
                  <Link to="/register"><b style={{ color: 'white' }}>About</b></Link>
                </div>
                <div className="navbar-item">
                  <Link to="/register"><b style={{ color: 'white' }}>Bakery</b></Link>
                </div>
                <div className="navbar-item">
                  <Link to="/register"><b style={{ color: 'white' }}>Delivery</b></Link>
                </div>
                <div className="navbar-item">
                  <Link to="/register"><b style={{ color: 'white' }}>Contact</b></Link>
                </div>
              </div>
            </div>
          </nav>
          <Carousel
            showArrows={false}
            showThumbs={false}
            showStatus={false}
            autoPlay
            interval={3000}
            infiniteLoop
            className="carousel"
          >
            <img src="/img/c2.jpg" style={{ height: 700, marginTop: -200 }} />
           
          </Carousel>
        </section>

        <section>
          <div className="columns">
            <div className="column is-2 is-offset-2 kue">
              <figure className="image is-1by1" >
                <img
                  className="is-rounded "
                  src="img/brownies.jpg"
                  style={{ objectFit: 'cover' }}
                />
                <div class="overlay">
                  <div class="text">Brownies</div>
                </div>
              </figure>
            </div>

            <div className="column is-2 is-offset-1 kue">
              <figure className="image is-1by1">
                <img
                  className="is-rounded"
                  src="img/lapis.jpg"
                />
                <div class="overlay">
                  <div class="text">Kue Lapis</div>
                </div>
              </figure>
            </div>
            <div className="column is-2 is-offset-1 kue">
              <figure className="image is-1by1">
                <img
                  className="is-rounded"
                  src="img/roll.jpg"
                />
                <div class="overlay">
                  <div class="text">Kue Roll</div>
                </div>
              </figure>
            </div>
          </div>
        </section>

        <section style={{ marginBottom: 300 }}>
          <div className="columns">
            <div className="column is-10 is-offset-1">
              <h1 style={{ color: 'chocolate', fontSize: 35, marginBottom: 100, textAlign: 'right' }}>
                Ingin membuat kue tapi ngga ada waktu ? <br />
                Ikut tutorial youtube tapi jadinya bantet ? <br />
                Jangan Khawatir, kami punya solusinya...<br />
                Tinggal klik kue langsung sampai rumah anda.<br />
              </h1>
              <div className="box brownies">
                <div className="columns" style={{ fontSize: 35, marginBottom: 100 }}>
                  <div className="column is-2 is-offset-3">
                    {
                      this.state.brownies ?
                        <span className="has-text-white" onClick={this.handleBrownies}>Brownies</span>
                        :
                        <span className="has-text-grey" onClick={this.handleBrownies}>Brownies</span>
                    }
                  </div>
                  <div className="column is-2">
                    {
                      this.state.lapis ?
                        <span className="has-text-white" onClick={this.handleLapis}>Kue Lapis</span>
                        :
                        <span className="has-text-grey" onClick={this.handleLapis}>Kue Lapis</span>
                    }
                  </div>
                  <div className="column is-2">
                    {
                      this.state.roll ?
                        <span className="has-text-white" onClick={this.handleRoll}>Kue Roll</span>
                        :
                        <span className="has-text-grey" onClick={this.handleRoll}>Kue Roll</span>
                    }
                  </div>
                </div>
                {
                  this.state.brownies ?
                    <Brownies />
                    :
                    <div></div>
                }
                {
                  this.state.lapis ?
                    <KueLapis />
                    :
                    <div></div>
                }
                {
                  this.state.roll ?
                    <KueRoll />
                    :
                    <div></div>
                }
              </div>
            </div>
          </div>
        </section>

        <section style={{ marginBottom: 300 }}>
          <img src="img/bahan1.jpg" style={{ width: '100%', height: 600 }} />
        </section>


          <section className="footer" style={{ background: '#bf9173' }}>
            <div className="columns">
              <div className="column is-7 menu">
                <Link to='/' style={{ color: 'white' }}><span>Home</span></Link>
                <span>   |   </span>
                <Link to='/about' style={{ color: 'white' }}><span>About</span></Link>
                <span>   |   </span>
                <span>Contact</span><br />
                <span>Privacy Policy</span>
              </div>

              <div className="column is-2 is-offset-2 has-text-right has-text-centered-mobile follow">
                <span>Follow Us</span>
                <div className="columns">
                  <div className="column is-12">
                    <span className="icon sosmed">
                      <i className="fab fa-facebook"></i>
                    </span>
                    <span className="icon sosmed">
                      <i className="fab fa-instagram"></i>
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </section>
      </div>
        );
      }
    }
    
    export default Header;
